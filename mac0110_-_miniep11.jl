using Unicode
function palindromo(string) 
    string = Unicode.normalize(lowercase(replace(string, r"[\-',!>;?/|<. ]" => "")), stripmark=true)
    if string == reverse(string)
        return true
    else
        return false
    end
end

using Test
function testes()
    @test palindromo("")
    @test palindromo("ovo")
    @test !palindromo("MiniEP11")
    @test palindromo("Socorram-me, subi no ^onibus em Marrocos!")
    @test palindromo("A mãe te ama.")
    @test palindromo("Roma me tem amor.")
    @test palindromo("A mala nada na lama.")
    @test palindromo("A grama é amarga.")
    @test palindromo("A Rita, sobre vovô, verbos atira.")
    @test palindromo("Olé! Maracujá, caju, caramelo!")
    @test palindromo("Rir, o breve verbo rir.")
    @test palindromo("Anotaram a data da maratona.")
    @test !palindromo("Salve, monitor!")
    @test !palindromo("Passei em MAC0110! Eu acho...")
    println("Fim dos testes!")
end
testes()
